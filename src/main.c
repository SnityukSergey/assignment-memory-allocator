#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "mem.h"
#include "util.h"
#include "mem_internals.h"

static void* heap;

static struct block_header *get_block_header(void *data) {
	return (struct block_header*) ((uint8_t*) data - offsetof(struct block_header, contents));
}

void test1() {
	printf("Test 1:\n");
	void* mem = _malloc(256);
	debug_heap(stderr, heap);
	_free(mem);
	printf("Passed\n");
}

void test2() {
	printf("Test 2\n");
	void* first = _malloc(100);
	void* second = _malloc(200);
	debug_heap(stdout, heap);
	_free(first);
	_free(second);
	printf("Passed\n");
}

void test3() {
	printf("Test 3\n");
	void* first = _malloc(100);
	void* second = _malloc(200);
	void* third = _malloc(300);
	_free(second);
	debug_heap(stdout, heap);
	_free(first);
	_free(third);
	printf("Passed\n");
}

void test4() {
	printf("Starting Test-4:\n");
	void* first = _malloc(10000);
	void* second = _malloc(10000);
	void* third = _malloc(10000);
	debug_heap(stdout, HEAP_START);
	_free(second);
	_free(first);
	_free(third);
	printf("Passed\n");
}

int main() {
	heap = heap_init(10000);
	test1();
	test2();
	test3();
	test4();
	return 0;
}
